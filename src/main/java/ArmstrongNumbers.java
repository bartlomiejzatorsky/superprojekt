import java.util.ArrayList;
import java.util.List;

public class ArmstrongNumbers {

    public static boolean isArmstrongNumber(int number) {

        int temp = number;
        List<Integer> digits = new ArrayList<Integer>();


        while (number > 0) {
            digits.add(number % 10);
            number /= 10;
        }

        double a = 0;
        for (int c : digits){
            a += Math.pow(c, digits.size());
        }

        return a == temp;

    }

}
